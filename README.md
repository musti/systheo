# Systemtheorie 2 Project
Here is the repo of our project.

- In the "Aufgaben" folder you will find our resulting figures.<br>
- Project_Model_2022a_WS2324.slx is the model we modified.<br>
- Systemtheorie2_Projekt_DE files are our protocol.<br>

- Group 34<br>
- Group Members:<br>
    - Mehmet Emin Fincan<br>
    - Doruk Polat<br>
    - Mert Hakan Salan<br>
    - Mustafa Turkoglu<br>

We hope that your reaction will not be the like this after you read our protocol:<br>
![Ripping Paper](https://media1.tenor.com/m/UZ5QdJThAckAAAAd/mcgregor-ripping-paper.gif)