function [] = reshape2()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
d=dir('fig*.fig');  % return list of figures 'figN.fig'
for i=1:length(d)
  hFig=openfig(d(i).name);
  xlim([0 10])
  ylim([-30 400])
  savefig(hFig, append('reshaped_', d(i).name))
  close(hFig)  
end
end

